from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import *
from .models import *

# Create your views here.
response = {}
def jadwal(request):
    form = detail_matkul()
    matkuls = Jadwal.objects.all()
    response['matkuls'] = matkuls
    response['form'] = form
    return render(request,'Jadwal.html', response)

def savejadwal(request):
    form = detail_matkul(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        response['mata_kuliah'] = request.POST['mata_kuliah']
        response['dosen'] = request.POST['dosen']
        response['jumlah_sks'] = request.POST['jumlah_sks']
        response['deskripsi'] = request.POST['deskripsi']
        response['semester'] = request.POST['semester']
        response['ruangan'] = request.POST['ruangan']
        matkul = Jadwal(mata_kuliah=response['mata_kuliah'], dosen=response['dosen'], jumlah_sks=response['jumlah_sks'], deskripsi=response['deskripsi'], semester=response['semester'], ruangan=response['ruangan'],)
        matkul.save()
        
        return HttpResponseRedirect('/story5')
    else:
        return HttpResponseRedirect('/')

def add_detail(request, myid):
    matkul = Jadwal.objects.get(id=myid)
    response['matkul'] = matkul
    return render(request,'mata_kuliah.html',response)

def delete_detail(request, myid):
    Jadwal.objects.get(id=myid).delete()
    return HttpResponseRedirect('/story5')
