from django import forms
from .models import Jadwal

class detail_matkul(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = ['mata_kuliah', 'dosen', 'jumlah_sks', 'deskripsi', 'semester', 'ruangan']
    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs1 = {
        'type' : 'text',
        'placeholder' : 'Nama mata kuliah',
        'style' : 'background-color: #E8EEF2'
    }
    input_attrs2 = {
        'type' : 'text',
        'placeholder' : 'Nama dosen',
        'style' : 'background-color: #E8EEF2'
    }
    input_attrs3 = {
        'type' : 'text',
        'placeholder' : 'Jumlah SKS',
        'style' : 'background-color: #E8EEF2'
    }
    input_attrs4 = {
        'type' : 'text',
        'placeholder' : 'Deskripsi mata kuliah',
        'style' : 'background-color: #E8EEF2'
    }
    input_attrs5 = {
        'type' : 'text',
        'placeholder' : 'Semester',
        'style' : 'background-color: #E8EEF2'
    }
    input_attrs6 = {
        'type' : 'text',
        'placeholder' : 'Nomor ruangan',
        'style' : 'background-color: #E8EEF2'
    }
    mata_kuliah = forms.CharField(label='Mata Kuliah', required=True, max_length=27, widget=forms.TextInput(attrs=input_attrs1))
    dosen = forms.CharField(label='Nama Dosen', required=True, max_length=27, widget=forms.TextInput(attrs=input_attrs2))
    jumlah_sks = forms.CharField(label='Jumlah SKS', required=True, max_length=27, widget=forms.TextInput(attrs=input_attrs3))
    deskripsi = forms.CharField(label='Deskripsi Mata Kuliah', required=True, max_length=27, widget=forms.Textarea(attrs=input_attrs4))
    semester = forms.CharField(label='Semester', required=True, max_length=27, widget=forms.TextInput(attrs=input_attrs5))
    ruangan = forms.CharField(label='Nomor Ruangan', required=True, max_length=27, widget=forms.TextInput(attrs=input_attrs6))


